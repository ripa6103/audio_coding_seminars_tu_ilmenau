import numpy as np


class Filter:

    def DirectMdct(self, x, N, window_func):
        L = 2 * N
        h_n = window_func  # window_func
        C = np.zeros((x.shape[0], N))  # create matrix 44100 x 128
        H = np.zeros((N, L))  # create matrix 128 x 256

        for k in np.arange(N):
            h_k = h_n * np.sqrt(2.0 / N) * np.cos(np.pi / N * (k + 0.5) * (np.arange(L) + 0.5 - N / 2))
            H[k, :] = h_k  # Column wise filling.
            a = np.convolve(h_k, x)
            C[:, k] = a[0:x.shape[0]]  # remove the fade out
        Y = C[::N]
        return Y, H

    def invDirectMdct(self, Y, N, window_func):
        L = window_func.shape[0]
        # upsample in python
        Y_upsampled = np.zeros((Y.shape[0] * N, N))
        Y_upsampled[::N, :] = Y

        G = np.zeros((N, L))
        x = np.zeros((Y_upsampled.shape[0], N))
        g_n = window_func
        for k in np.arange(N):
            g_k = g_n * np.sqrt(2.0 / N) * np.cos(np.pi / N * (k + 0.5) * (np.arange(L) + 0.5 - N / 2))
            G[k, :] = g_k
            a = np.convolve(g_k[::-1], Y_upsampled[:, k])  # Flip the synthesis with g_k[::-1]
            x[:, k] = a[0: Y_upsampled.shape[0]]  # remove the fade out
            # Does the same as:
            # y[:, k] = lfilter(g_k, 1, Y[:, k])  # Column wise filling.

        x_hat = np.sum(x, axis=1)
        return x_hat, G

    def generateInput(self, N):
        Impulse = np.zeros(N)
        Impulse[0] = 1
        return Impulse
