import numpy as np
import matplotlib.pyplot as plt
from seminar_3.mdct_sem_3 import MDCT_Filter
from scipy.signal import freqz
from seminar_1.audio_utility import AudioUtils
from pathlib import Path

audio_utils = AudioUtils()
mdct_filter = MDCT_Filter()

# Number of subbands:
N = 256
L = 2 * N

# Filter bank coefficients for sine window:
# fb = np.sin(np.pi/(2*N)*(np.arange(int(1.5*N))+0.5))

# input test signal, ramp:
# x = np.arange(128)
x = np.arange(44100) / 44100
plt.plot(x)
plt.title('Input Signal - Ramp')
plt.xlabel('Sample')
plt.show()

size = x.shape[0]
# create window function
window_func = mdct_filter.window_function(N)

# MDCT
Y, H = mdct_filter.DirectMdct(x, N, window_func)
print(Y.shape, H.shape)
# plt.imshow(20 * np.log10(abs(Y)))
# plt.title('MDCT Subbands')
# plt.xlabel('Block No.')
# plt.ylabel('Subband No.')
# plt.show()


# Reconstructed
reconstructed, a = mdct_filter.invDirectMdct(Y, N, window_func)

plt.figure()
plt.plot(reconstructed)
plt.title('Reconstructed Signal')
plt.xlabel('Sample')
plt.show()

plt.figure()
plt.plot(x, 'b')
plt.hold = True
plt.plot(reconstructed, 'r')
plt.title('original signal - blue;  perfect reconstruction - red')
# plt.xlabel('t in [s]')
# plt.ylabel('Amplitude')
# plt.axis([0, size/2, 0, 1])
plt.show()

Impulse = mdct_filter.generateInput(N)
h_k = window_func * np.sqrt(2.0 / N) * np.cos(np.pi / N * (0 + 0.5) * (np.arange(L) + 0.5 - N / 2))
# H[k, :] = h_k  # Column wise filling.
Y_from_impulse = np.convolve(h_k, Impulse, 'full')
# Y_from_impulse, H2 = f.DirectMdct(Impulse, N, window_func)

# Plot Impulse response
print(Y_from_impulse.shape)
plt.figure()
plt.plot(Y_from_impulse)
plt.title('Impulse response')
plt.xlabel('n in samples')
plt.ylabel('Amplitude')
plt.show()

# Plot frequency response
# print(H2.shape)
# wfreq, Hfreq = freqz(H2[0, :])  # freqz in Python
wfreq, Hfreq = freqz(h_k)
# plt.figure()
# plt.title('frequency response')
# plt.xlabel('frequency')
# plt.ylabel('Amplitude in dB')
# plt.plot(wfreq, 20 * np.log10(abs(Hfreq)))
# plt.show()

root_path = Path(__file__).parent.parent
samplerate, wavearray = audio_utils.read_wave_file(Path.joinpath(root_path, "Track16.wav"))
testSignal = wavearray[:, 0]
testSignal = testSignal[:16000]
YTest, HTest = mdct_filter.DirectMdct(testSignal, N, window_func)
Yreconstructed, aTest = mdct_filter.invDirectMdct(YTest, N, window_func)
plt.plot(testSignal)
plt.plot(Yreconstructed)
plt.show()
