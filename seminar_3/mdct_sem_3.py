from seminar_3.MDCT import Filter, np

class MDCT_Filter(Filter):

    def window_function(self, filterLength):
        L = 2 * filterLength
        window = np.sin(np.pi / L * (np.arange(L) + 0.5))
        return window