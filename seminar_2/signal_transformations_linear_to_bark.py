import numpy as np
import scipy.signal as scsig


class LinearToBarkUtilityClass:

    def __init__(self, alpha=0.6):
        self.alpha = alpha # 0.3 works best for psycho acoustic models since we use voltage we multiply it by 2

    def generate_signal(self, frequency=None, sampling_frequency=None, durationMins=None):
        assert durationMins is not None or sampling_frequency is not None or frequency is not None, ValueError(
            "Invalid Parameters")
        duration = 60 * durationMins
        generatedSignal = np.sin(
            2 * np.pi * np.arange(sampling_frequency * duration) * (frequency / sampling_frequency))
        # generatedSignal = np.sin(
        #     2 * np.pi / (2 * DFTResolution) * frequency * np.arange(self.SamplingFrequency * duration)) * 1000
        return generatedSignal

    def generate_signal_dft_resol(self, frequency=None, sampling_frequency=None, DFTResolution=None, durationMins=None):
        """ This function Generates a signal based on the DFT resolution
        One could generate a signal based on normalised frequency sin(2*pi*f*t) but for this seminar we need a
        signal not at a particular frequency rather particular point bin of the DFT resolution so
        sin(2*pi*f/(2*DFT_Resol)*t)"""
        assert durationMins is not None or sampling_frequency is not None or frequency is not None, ValueError(
            "Invalid Parameters")
        if DFTResolution is None:
            DFTResolution = 1024

        duration = 60 * durationMins
        # generatedSignal = np.sin(2*np.pi*np.arange(self.SamplingFrequency * duration)*(frequency/self.SamplingFrequency))
        generatedSignal = np.sin(
            2 * np.pi / (2 * DFTResolution) * frequency * np.arange(sampling_frequency * duration)) * 1000
        return generatedSignal

    def frequency_hz_to_bark(self, f):
        # Convert Frequency to Bark scale as ears do not operate on a linear frequency scale
        return (6. * np.arcsinh(f / 600.))  # f in Hz

    def compute_STFT(self, signal, sampling_frequency, subbands=2048):
        # Devide signal into subbands to do processing of different bands differently
        sampleFrequency, segmentTime, zxx = scsig.stft(signal, sampling_frequency, nperseg=subbands)
        return sampleFrequency, segmentTime, zxx

    def getBarkStepSize(self, barkFrequencyMax, barkResolution):
        return barkFrequencyMax / (barkResolution - 1)

    def getQuantizedBark(self, barkFrequency, barkResolution):
        # Bark scale quantization is required in order to process bunch of similar frequencies
        return np.round(barkFrequency / self.getBarkStepSize(barkFrequency.max(), barkResolution))  #

    def mappingFreq2BarkBands(self, barkResolution, DFTresolution, quantizedBark):
        # Map Freq to bark bands
        barkFreqMapMat = np.zeros((barkResolution, DFTresolution + 1))
        for i in range(barkResolution):
            barkFreqMapMat[i, :] = (quantizedBark == i)
        return barkFreqMapMat  # matrix of size 48X1025

    def mapDFTbandsToBarkbands(self, magnitudeSpectrum, mappingMatrix):
        psd = magnitudeSpectrum**2
        return np.dot(psd, mappingMatrix.T) # within This is adding the power of the subbands, vector of 1x48

    def mappingbackBark2Freq(self, barkFreqMapMat):
        return  np.dot(np.diag((1.0/np.sum(barkFreqMapMat,axis=1))**0.5), barkFreqMapMat).T

    def spreadingFunctionDB(self, barkResolution, frequency):
        # maxbark = self.FrequencyHzToBark(self.SamplingFrequency/2)
        spreadingVectorDB = np.zeros(2*barkResolution)
        # value of simultaneous masking with all assumption mentioned in book = 23.5
        simultaneousMasking = 23.5
        # prototype --> bark bands for lower slope
        # 27 is the value for S1
        spreadingVectorDB[0:barkResolution] = np.linspace(-frequency*27, -8, barkResolution) - simultaneousMasking
        # prototype --> bark bands for upper slope
        # 12 is the value for s2
        spreadingVectorDB[barkResolution : 2*barkResolution] = \
            np.linspace(0,-frequency*12, barkResolution) - simultaneousMasking
        return spreadingVectorDB

    def spreadingMatFromPrototype(self, spreadFuncPrototypeDB, barkResouution):
        #convert db value to voltage
        spreadVectorVoltage = 10.0**(spreadFuncPrototypeDB/20.0*self.alpha)
        # Create a matrix which will hold all bark scale spreading functions
        spreadingMatrixVoltage = np.zeros((barkResouution, barkResouution))
        for i in range(barkResouution):
            spreadingMatrixVoltage[i,:] = spreadVectorVoltage[(barkResouution - i):(2*barkResouution - i)]
        return spreadingMatrixVoltage

    def maskingThresholdBark(self, magnitudeInBark, spreadingMatrix):
        mTbark = np.dot(magnitudeInBark**self.alpha, spreadingMatrix**self.alpha)
        mTbark = mTbark**(1.0/self.alpha)
        return mTbark

    def maskingThreshold(self, magSpec, barkFreqMapMat, spreadingMatrix):
        power_of_subbands_added = self.mapDFTbandsToBarkbands(magSpec, barkFreqMapMat)
        barkIntesity = power_of_subbands_added**0.5
        mTbark = self.maskingThresholdBark(barkIntesity, spreadingMatrix)
        return mTbark

    def mappingfrombark(self, mTbark, barkFreqMapMatInv):
        mT = np.dot(mTbark, barkFreqMapMatInv.T)
        return mT