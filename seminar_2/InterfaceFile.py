from signal_transformations_linear_to_bark import LinearToBarkUtilityClass
import matplotlib.pyplot as plt
import numpy as np


# def plotSignalTask1(timescale, addedSignal):
#     plt.plot(timescale, addedSignal)
#     plt.xlabel("Seconds")
#     plt.ylabel("Amplitude")
#     plt.show()
#     return
#
#
def plotSTFT(time, freq, mag):
    plt.pcolormesh(time, freq, np.abs(mag))
    plt.title('Spectogram')
    plt.xlabel('Time')
    plt.ylabel('Frequency')
    plt.show()
    return


def DFTMagSpecLinScale(freq):
    mX = np.abs(freq)
    plt.plot(mX)
    plt.title('Example DFT Magnitude Spectrum on Linear Scale')
    plt.xlabel('Frequency')
    plt.ylabel('Amplitude')
    plt.show()


def plotFreqToBarkMapMat(FreqBarkMapMat):
    plt.imshow(FreqBarkMapMat[:, :300])
    plt.title('Matrix for Uniform to Bark Mapping')
    plt.xlabel('Uniform Subbands')
    plt.ylabel('Bark Subbands')
    plt.show()


def plotMagInBarkBand(freqInBark):
    plt.plot(freqInBark)
    plt.title('Magnitude spectrum mapped to 1/2 bark')
    plt.xlabel('Frequency in Bark')
    plt.ylabel('Amplitude in dB')
    plt.show()


def plotPrototypeSpreadingFunction(spreadingVector):
    plt.plot(spreadingVector)
    plt.xlabel('Bark Bands')
    plt.ylabel('Sound pressure level in dB')
    plt.title('Spreading Function')
    plt.show()


def plotSpreadingMatrix(spreadingMatrix):
    plt.imshow(spreadingMatrix)
    plt.title('Spreading Matrix')
    plt.xlabel('Bark Subbands')
    plt.ylabel('Bark Subbands')
    plt.show()


def plotMaskingThresholdBark(mtBarkDomain):
    plt.plot(mtBarkDomain)
    plt.title('Masking Threshold in Bark Domain')
    plt.xlabel('Bark Band')
    plt.show()


def plotBarkToFreqMapMat(barkToFreqMapMat):
    plt.imshow(barkToFreqMapMat[:300, :])
    plt.title('Matrix for Bark to Uniform Mapping')
    plt.xlabel('Bark Subbands')
    plt.ylabel('Uniform Subbands')
    plt.show()


def plotMaskingTInLinDomain(maskingTLinDomain):
    plt.plot(maskingTLinDomain)
    plt.title('Masking Theshold back in Linear Domain')
    plt.show()


def plotingMaskingTForTones(maskingThreshHold, magSpecturm):
    plt.plot(20 * np.log10(maskingThreshHold + 1e-3))
    plt.title('Masking Threshold for 200 and 600 hz')
    plt.plot(20 * np.log10(np.abs(magSpecturm) + 1e-3))
    plt.legend(('Masking Trheshold', 'Magnitude Spectrum Tone'))
    plt.xlabel('FFT subband')
    plt.ylabel("dB")
    plt.show()


if __name__ == '__main__':
    barkResolution = 48
    DFTResolution = 1024
    sampleFrequency = 44100
    # sig = LinearToBarkUtilityClass()
    sig = LinearToBarkUtilityClass()

    # Generating two signals --> Task 1 with sampling Freq 200 and 600 and Apply STFT
    twoHundredHzSig = sig.generate_signal_dft_resol(200, 44100, DFTResolution, 3)
    sixHundredHxSignal = sig.generate_signal_dft_resol(600, 44100, DFTResolution, 3)

    # twoHundredHzSig = sig.generateSignal(200, DFTResolution, 3)
    # sixHundredHxSignal = sig.generateSignal(600, DFTResolution, 3)
    # # twoHundredHzSig = sig.getSigforSpectogram(200)
    # # sixHundredHxSignal = sig.getSigforSpectogram(600)

    addedSignal = twoHundredHzSig + sixHundredHxSignal
    # timescale, addedSignal = sig.addTwoSignals(twoHundredHzSig, sixHundredHxSignal)
    sampleFrequency, segmentTime, zxx = sig.compute_STFT(addedSignal, sampleFrequency, subbands=int(DFTResolution * 2))

    # sampleFrequency, segmentTime, zxx = sig.CalculateSTFT(addedSignal, subbands=int(DFTResolution * 2))
    magSpectrum = zxx[:, 1]  # since frequency exist in all time so if we take just 1 column its enough

    # Transformation of STFT to BARK scale --> Task 2
    barkFrequency = sig.frequency_hz_to_bark(
        sampleFrequency)  # here I have all 1024 subbands of frequency converted to bark

    # barkFrequency = sig.FrequencyHzToBark(
    #     sampleFrequency)  # here I have all 1024 subbands of frequency converted to bark

    quantizedBark = sig.getQuantizedBark(barkFrequency, barkResolution)  # array of 1025 elements, quantized
    freqbarkMapMat = sig.mappingFreq2BarkBands(barkResolution=barkResolution,
                                               quantizedBark=quantizedBark, DFTresolution=DFTResolution)  # 48x1025

    # Frequency to bark mapping matrix --> barkFreqMapMat

    addedMagSpectrum = np.sum(np.abs(zxx), axis=1)  # This is not required anymore

    '''For the actual mapping from the magnitude of complex uniformly spaced DFT subbands for the pschyco acoustic
   model of the bark subbands we add the signal powers from the corresponding DFT bands. We can take the square
   root of it to obtain the voltage'''

    magMappedBark = sig.mapDFTbandsToBarkbands(np.abs(magSpectrum),
                                               freqbarkMapMat)  # power of each freq band added, 1x48
    mxBark = magMappedBark ** 0.5  # array of 1x48 frequency magnitude converted to bark

    spreadingVectorInDb = sig.spreadingFunctionDB(barkResolution,
                                                  sig.frequency_hz_to_bark(sampleFrequency.max()))  # Vector of 1x2*48

    # spreadingVectorInDb = sig.spreadingFunctionDB(barkResolution,
    #                                               sig.FrequencyHzToBark(sampleFrequency.max()))  # Vector of 1x2*48

    spreadingMatrix = sig.spreadingMatFromPrototype(spreadingVectorInDb, barkResolution)  # 48x48 matrix

    mTBark = sig.maskingThresholdBark(mxBark, spreadingMatrix)  # 1x48 vector masking t

    barkToFreqMatInv = sig.mappingbackBark2Freq(freqbarkMapMat)

    mt = sig.mappingfrombark(mTBark, barkToFreqMatInv)

    f = np.linspace(0, sampleFrequency.max() / 2, DFTResolution + 1) + 1e-10
    tfreq = sampleFrequency / 21.5

    sig1 = sig.generate_signal(200, 44100, 3)
    sig2 = sig.generate_signal(600, 44100, 3)
    addedsig = sig1, sig2
    freq, time, mag = sig.compute_STFT(addedsig, 44100, subbands=int(DFTResolution * 2))

    LTQ = np.clip((3.64 * (f / 1000.) ** -0.8 - 6.5 *
                   np.exp(-0.6 * (f / 1000. - 3.3) ** 2.)
                   + 1e-3 * ((f / 1000.) ** 4.)), -20, 80)
    mt = np.max((mt, 10.0 ** ((LTQ - 60) / 20)), 0)

    # outputs
    # plotSTFT(segmentTime, tfreq, zxx)
    # DFTMagSpecLinScale(magSpectrum)
    # plotFreqToBarkMapMat(freqbarkMapMat)
    # plotMagInBarkBand(mxBark)
    # plotSpreadingMatrix(spreadingMatrix)
    # plotPrototypeSpreadingFunction(spreadingVectorInDb)
    # plotMaskingThresholdBark(mTBark)
    # plotBarkToFreqMapMat(barkToFreqMatInv)
    # plotMaskingTInLinDomain(mt)
    # plotingMaskingTForTones(mt, magSpectrum)
