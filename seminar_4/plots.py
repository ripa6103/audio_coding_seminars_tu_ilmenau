import matplotlib.pyplot as plt
import numpy as np

class PlotLib:

    def DFTMagSpecLinScale(self, freq):
        mX = np.abs(freq)
        plt.plot(mX)
        plt.title('Example DFT Magnitude Spectrum on Linear Scale')
        plt.xlabel('Frequency')
        plt.ylabel('Amplitude')
        plt.show()

    def plotFreqToBarkMapMat(self, FreqBarkMapMat):
        plt.imshow(FreqBarkMapMat[:, :300])
        plt.title('Matrix for Uniform to Bark Mapping')
        plt.xlabel('Uniform Subbands')
        plt.ylabel('Bark Subbands')
        plt.show()

    def plotMagInBarkBand(self, freqInBark):
        plt.plot(freqInBark)
        plt.title('Magnitude spectrum mapped to 1/2 bark')
        plt.xlabel('Frequency in Bark')
        plt.ylabel('Amplitude in dB')
        plt.show()

    def plotPrototypeSpreadingFunction(self, spreadingVector):
        plt.plot(spreadingVector)
        plt.xlabel('Bark Bands')
        plt.ylabel('Sound pressure level in dB')
        plt.title('Spreading Function')
        plt.show()

    def plotSpreadingMatrix(self, spreadingMatrix):
        plt.imshow(spreadingMatrix)
        plt.title('Spreading Matrix')
        plt.xlabel('Bark Subbands')
        plt.ylabel('Bark Subbands')
        plt.show()

    def plotMaskingThresholdBark(self, mtBarkDomain):
        plt.plot(mtBarkDomain)
        plt.title('Masking Threshold in Bark Domain')
        plt.xlabel('Bark Band')
        plt.show()

    def plotBarkToFreqMapMat(self, barkToFreqMapMat):
        plt.imshow(barkToFreqMapMat[:300, :])
        plt.title('Matrix for Bark to Uniform Mapping')
        plt.xlabel('Bark Subbands')
        plt.ylabel('Uniform Subbands')
        plt.show()

    def plotMaskingTInLinDomain(self, maskingTLinDomain):
        plt.plot(maskingTLinDomain)
        plt.title('Masking Theshold back in Linear Domain')
        plt.show()

    def plotingMaskingTForTones(self, maskingThreshHold, magSpecturm):
        plt.plot(20 * np.log10(maskingThreshHold + 1e-3))
        plt.title('Masking Threshold for 200 and 600 hz')
        plt.plot(20 * np.log10(np.abs(magSpecturm) + 1e-3))
        plt.legend(('Masking Trheshold', 'Magnitude Spectrum Tone'))
        plt.xlabel('FFT subband')
        plt.ylabel("dB")
        plt.show()