from seminar_3.MDCT import Filter, np

class MDCT_Filter(Filter):

    def window_function(self, filterLength):
        window = np.sin(np.pi / filterLength * (np.arange(filterLength) + 0.5))
        return window