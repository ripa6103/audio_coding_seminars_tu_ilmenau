from seminar_1.audio_utility import AudioUtils
from seminar_2.signal_transformations_linear_to_bark import LinearToBarkUtilityClass
from seminar_4.mdct_sem_4 import MDCT_Filter, np
import scipy.signal as scisig
from seminar_4.plots import PlotLib
from pathlib import Path

def analysisAndQuantisation(sampleFrequency, ys, AnalysisFilterResult, DFTResolution, barkResolution, maxFreq):
    # First creating band of linear frequencies and then converting them to bark domain
    barkFrequency = mySig.frequency_hz_to_bark(sampleFrequency)
    quantizedBark = mySig.getQuantizedBark(barkFrequency, barkResolution)# Now quantizing the bark

    # Now creating the freq to bark mapping matrix
    freqbarkMapMat = mySig.mappingFreq2BarkBands(barkResolution, DFTResolution-1, quantizedBark) # The W mat
    barkToFreqMatInv = mySig.mappingbackBark2Freq(freqbarkMapMat) # The inverse of W mat

    # Spreading vector and Matrix
    spreadingVectorInDb = mySig.spreadingFunctionDB(barkResolution, mySig.frequency_hz_to_bark(maxFreq))
    spreadingMatrix = mySig.spreadingMatFromPrototype(spreadingVectorInDb, barkResolution)

    # One row dotted with a matrix gives a vetor in row format
    # ys is the actual fourier coeffs --> I take the abs of it
    ysabs = np.abs(ys)
    ysabs = ysabs.T  # This is done to make the dot product work

    magMappedBark = mySig.mapDFTbandsToBarkbands(ysabs, freqbarkMapMat)
    mxBark = magMappedBark ** 0.5  # getting in form of voltage
    # each row of the mxbark contains the magnitude spectrum in bark, plt.plot(mxBark.T) plots the mag spec

    mTBark = mySig.maskingThresholdBark(mxBark, spreadingMatrix)
    # Each row of mTBark contains the masking threshhold
    mTBarkQuantized = np.round(np.log2(mTBark) * 4)  # Quantisation of the scale factors
    mTBarkQuantized = np.clip(mTBarkQuantized, 0, None)
    mTBarkDeQuantized = np.power(2, mTBarkQuantized / 4)  # dequantisation of the scale factors
    mT = mySig.mappingfrombark(mTBarkDeQuantized, barkToFreqMatInv)

    print("Quantising according to masking threshold")
    delta = mT * 2
    delta = delta[:-1, :]  # leaving the last data to match dimension with output of the MDCT analysis Filterbank
    yq = np.round(AnalysisFilterResult / delta)

    return yq, mTBarkQuantized, mT

def dequantisationAndSysthesis(yq, mTBarkQuantized, DFTResolution,sampleFrequency, windowFunc):
    mTBarkDeQuantized = np.power(2, mTBarkQuantized/4)

    barkFrequency = mySig.frequency_hz_to_bark(sampleFrequency)
    quantizedBark = mySig.getQuantizedBark(barkFrequency, barkResolution)

    # Now creating the freq to bark mapping matrix
    freqbarkMapMat = mySig.mappingFreq2BarkBands(barkResolution, DFTResolution-1, quantizedBark)  # The W mat
    barkToFreqMatInv = mySig.mappingbackBark2Freq(freqbarkMapMat)  # The inverse of W mat

    mT = mySig.mappingfrombark(mTBarkDeQuantized, barkToFreqMatInv)
    delta = mT * 2
    delta = delta[:-1, :]
    ydeq = yq * delta
    xrek, G = dirMDCT.invDirectMdct(ydeq, DFTResolution, windowFunction)
    return xrek, G

#declarations
root_path = Path(__file__).parent.parent
audio_utils = AudioUtils()
sRate, wavArray = audio_utils.read_wave_file(Path.joinpath(root_path, "Track16.wav"))
mySig = LinearToBarkUtilityClass(alpha=0.8)
dirMDCT = MDCT_Filter()
myPlotLib = PlotLib()
DFTResolution = 256 # This value was given in seminar 3
FilterLength = 2*DFTResolution # Due to above the filter length = 2*DFTResolution
barkResolution = 48 #The bark resolution is said to be kept same as seminar 2
quantisationBit = 8 # How many bits in the quantised signal
maxFreq = sRate/2
params1 = {'sample_rate': sRate,
              'length': len(wavArray),
              'dataType': wavArray.dtype,
              'channels': 2,
              'data': wavArray}

params2 = {'sample_rate': sRate,
              'length': len(wavArray),
              'dataType': wavArray.dtype,
              'channels': 2,
              'data': wavArray}

windowFunction = dirMDCT.window_function(FilterLength)

# duration = 5 #in seconds
# start = 10
# extractedAudio = mywav.extractAudio(start, duration, params)
# signalToModify = extractedAudio[:,0] #This signal will go to MDCT filter bank


# Passing the signal to the analysis filter bank
noOfSamplesyouwant = 100000
datafromNow = wavArray[:noOfSamplesyouwant, :]
rows = int(np.ceil(len(datafromNow)/DFTResolution)*DFTResolution)
reconstructedData = np.zeros((rows,2))

for i in range(0,2):
    signalToModify = datafromNow[:,i]
    AnalysisFilterResult, ImpulseResonseMat = dirMDCT.DirectMdct(signalToModify, DFTResolution, windowFunction)
    sampleFrequency, t, ys = scisig.stft(signalToModify, fs=2 * np.pi, nperseg=FilterLength - 1)
    ys *= np.sqrt(DFTResolution) / 2 / 0.375  # From book pg125, to match up parsevals energy

    quantisedResult, mtBarkQuantised, mT = analysisAndQuantisation(sampleFrequency, ys,
                            AnalysisFilterResult, DFTResolution, barkResolution, maxFreq)

    reconstructedResult, GMatrix = dequantisationAndSysthesis(quantisedResult,
                            mtBarkQuantised, DFTResolution, sampleFrequency, windowFunction)
    reconstructedData[:,i] = reconstructedResult



params1['length'] = len(datafromNow)
params1['channels'] = 2
params1['data'] = datafromNow
audio_utils.play_audio_from_wavedata(params1)


params2['length'] = len(reconstructedData)
params2['channels'] = 2
params2['data'] = reconstructedData
audio_utils.play_audio_from_wavedata(params2)