import heapq
import collections
import numpy as np

class HeapNode:
    def __init__(self, char, freq):
        self.char = char
        self.freq = freq
        self.left = None
        self.right = None

        return

    def __lt__(self, other):
        return self.freq < other.freq

    def __eq__(self, other):
        if (other == None):
            return False
        if (not isinstance(other, HeapNode)):
            return False
        return self.freq == other.freq

class HuffmanCoding:

    def __init__(self):
        self.heap = []
        self.codes = {}
        self.reverse_mapping = {}

    def makeFreqDictionary(self, data):
        frequency = collections.Counter(data)
        return frequency

    def makeNodes(self, frequency):
        for key in frequency:
            node = HeapNode(key, frequency[key])
            heapq.heappush(self.heap, node)

        return

    def mergeNodesGreedy(self):
        while(len(self.heap) > 1):
            node1 = heapq.heappop(self.heap)
            node2 = heapq.heappop(self.heap)

            merged = HeapNode(None, node1.freq + node2.freq)
            merged.left = node1
            merged.right = node2

            heapq.heappush(self.heap, merged)

        return

    def helpAssignLeftRightCodes(self, root, current_code):
        if(root == None):
            return

        if(root.char != None):
            self.codes[root.char] = current_code
            self.reverse_mapping[current_code] = root.char
            return

        self.helpAssignLeftRightCodes(root.left, current_code + "0")
        self.helpAssignLeftRightCodes(root.right, current_code + "1")

    def makeCodes(self):
        root = heapq.heappop(self.heap)
        current_code = ""
        self.helpAssignLeftRightCodes(root, current_code)

    def getDataEncoded(self, text):
        encoded_text = ""
        for character in text:
            encoded_text += self.codes[character]
        return encoded_text

    def getDataStorableByPadding(self, encoded_text):
        extra_padding = 8 - len(encoded_text)%8
        for i in range(extra_padding):
            encoded_text += "0"

        padded_info = "{0:08b}".format(extra_padding)
        encoded_text = padded_info + encoded_text
        return encoded_text

    def convertPaddedDataToByteArray(self, padded_encoded_text):
        if(len(padded_encoded_text) % 8 != 0):
            print("Encoded text not padded properly")
            exit(0)

        b = bytearray()
        for i in range(0, len(padded_encoded_text), 8):
            byte = padded_encoded_text[i:i+8]
            b.append(int(byte, 2))
        return b

    def compress(self, data):
        frequency = self.makeFreqDictionary(data)
        self.makeNodes(frequency)
        self.mergeNodesGreedy()
        self.makeCodes()
        encoded_text = self.getDataEncoded(data)
        padded_encoded_text = self.getDataStorableByPadding(encoded_text)
        b = self.convertPaddedDataToByteArray(padded_encoded_text)
        return frequency, self.reverse_mapping, encoded_text, padded_encoded_text, b

    def getPaddedTextFromByteArray(self, bytearray):
        bit_string = ""
        for byte in bytearray:
            bits = bin(byte)[2:].rjust(8, '0')
            bit_string += bits
        return bit_string

    def getEncodedTextFromPaddedText(self, paddedEncodedText):
        paddedInfo = paddedEncodedText[:8]
        extraPadding = int(paddedInfo, 2)
        paddedEncodedText = paddedEncodedText[8:]
        encodedText = paddedEncodedText[:-1*extraPadding]
        return encodedText

    def decodeEncodedText(self, encodedText):
        currentText = ""
        decodedUintArray = np.array([], dtype=np.uint8)

        for bit in encodedText:
            currentText += bit
            if(currentText in self.reverse_mapping):
                character = self.reverse_mapping[currentText]
                decodedUintArray = np.append(decodedUintArray, character)
                currentText = ""
        return decodedUintArray

    def decompress(self, bytearray):
        paddedString = self.getPaddedTextFromByteArray(bytearray)
        encodedString = self.getEncodedTextFromPaddedText(paddedString)
        decodedData = self.decodeEncodedText(encodedString)
        return decodedData