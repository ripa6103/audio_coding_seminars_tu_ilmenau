import pyaudio
import scipy.io.wavfile as wav
import numpy as np


class AudioUtils:

    def read_wave_file(self, filename):
        sample_rate, wave_array = wav.read(filename)
        return sample_rate, wave_array

    def play_audio(self, filename):
        sample_rate, wave_array = self.read_wave_file(filename)
        self.play_audio_from_wavedata({
            'sample_rate': sample_rate,
            'channels': wave_array.shape[1],
            'data': wave_array})

    def play_audio_from_wavedata(self, params):
        """
        1. get pyaudio instance
        2. open stream
        3. write stream
        4. stop stream
        5. close stream
        6. terminate audio instance
        :param params: a dictionary. keys(datatype) = sample_rate(int), channels(int), data(numpy.ndarray)
        :return:
        """
        p = pyaudio.PyAudio()
        stream = p.open(format=pyaudio.paInt16, channels=params['channels'],
                        rate=params['sample_rate'], output=True)
        stream.write(params['data'].astype(np.int16).tostring())
        stream.stop_stream()
        stream.close()
        p.terminate()

    @staticmethod
    def normalise_data(data):
        return data / np.max(np.abs(data))

    @staticmethod
    def extract_audio_chunck(wave_array, sample_rate, start_time, duration):
        start_sample = start_time * sample_rate
        duration_sample = duration * sample_rate
        extracted_wave = wave_array[start_sample:duration_sample + start_sample]
        return extracted_wave

    @staticmethod
    def create_wave_file(filename, sample_rate, wave_array):
        if filename is not None and sample_rate is not None:
            wav.write(open(filename, 'wb'), sample_rate, wave_array)
        else:
            print("Please provide all necessary parameters")
