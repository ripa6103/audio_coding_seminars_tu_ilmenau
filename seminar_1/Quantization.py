import numpy as np


class Quantization:

    def __init__(self, quant_type='dumb'):
        self.__signal = None
        self.__timescale = None
        self.__fS = None
        self.quant_type = quant_type

    def set_signal(self, signal):
        self.signal = signal

    def set_time_scale(self, time_scale):
        self.timescale = time_scale

    def set_sampling_frequency(self, fS):
        self.__fS = fS

    def __check_signal_valid(self, signal):
        assert signal is not None, ValueError("Signal is None. Please provide a signal")

    def __flatten_signal(self, signal):
        return signal.flatten()

    def __calculate_time_scale(self, signal):
        self.__check_signal_valid(signal)
        signal_samples = len(signal.flatten())  # signal can also come as a column vector. Good to flatten it before
        if self.__fS is None:
            return np.arange(signal_samples)
        return np.arange(0, signal_samples / self.__fS, 1 / self.__fS)

    def __calculate_step_size(self, signal, bits=16):
        self.__check_signal_valid(signal)
        signal = self.__flatten_signal(signal)
        difference = int(signal.max()) - int(signal.min())
        return difference / (2 ** bits)

    def quantize_signal(self, signal, param):
        # param is precision for dumb_quant and bits for smart quant
        # by default it will do smart quant
        self.__check_signal_valid(signal)
        if self.quant_type == 'dumb':
            quantized_signal = self.__dumb_quantize_signal(signal, param)
            return quantized_signal

        assert param == 16 or param == 8, ValueError(
            "Param represents quantization bit. At the moment supported for 8 and 16 bits")
        return self.__smart_quantize_signal(signal, param)


    def __dumb_quantize_signal(self, signal, precision):
        signal = self.__flatten_signal(signal)
        return np.round(signal, precision)

    def __smart_quantize_signal(self, signal, bits):
        # signal = self.__flatten_signal(signal)
        stepsize = self.__calculate_step_size(signal, bits)
        signal_quant_index = np.round(signal/stepsize)

        if bits == 8:
            signal_quant_index = signal_quant_index + 128 # Step for macbook, if you hear noise you can remove this
            quantized_signal = signal_quant_index.astype(np.uint8)
        else:
            quantized_signal = signal_quant_index.astype(np.int16)
        return quantized_signal, stepsize

