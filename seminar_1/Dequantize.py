class Dequantization:

    def __init__(self):
        pass

    def dequantize(self, quantized_signal, bits, stepsize):
        if bits == 16:
            reconstructed16Bit = quantized_signal * stepsize
            return reconstructed16Bit
        elif bits == 8:
            shifted_quantized_signal = quantized_signal - 255 / 2 # manually shift the signal to make it signed
            reconstructed8Bit = shifted_quantized_signal * stepsize
            return reconstructed8Bit