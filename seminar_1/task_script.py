from audio_utility import AudioUtils
import numpy as np
import matplotlib.pyplot as plt
from Quantization import Quantization
from Dequantize import Dequantization
import os

audio_utils = AudioUtils()
sample_rate, wave_array = audio_utils.read_wave_file('Track16.wav')
params = {
    'sample_rate': sample_rate,
    'channels': wave_array.shape[1],
    'data': wave_array}
# audio_utils.play_audio_from_wavedata(params)

# extracting portion of sound track
start_time = 9
duration_of_wave = 8

extracted_wave_array = audio_utils.extract_audio_chunck(wave_array, sample_rate, start_time, duration_of_wave)
extracted_normalised_array = audio_utils.normalise_data(extracted_wave_array)

WavArrayLeftCh = np.ascontiguousarray(extracted_wave_array[:, 0], np.int16)
WavArrayRightCh = np.ascontiguousarray(extracted_wave_array[:, 1], np.int16)

# FFT analysis
window_length = 1024
num_windows = 4

# Newbies to python can avoid for loops by following the below steps
channel_4096 = WavArrayRightCh[0:window_length * num_windows]
channel_4096 = channel_4096.reshape(num_windows, window_length)  # Each row contains 1024 samples for a data chunk

sampling_time = 1 / sample_rate
freq = np.fft.fftfreq(window_length, d=sampling_time)
freq = freq[range(int(window_length / 2))] / 1000 # Only positive frequency are relevant and convert to KHz

Y = np.fft.fft(channel_4096).real/window_length # Only take real part of spectrum and normalize
# In the above step if you fft over a matrix the fft is computed along axis 1

quant_instance = Quantization('smart')
quant_sig_16bit, step_size_16bit = quant_instance.quantize_signal(wave_array, 16)
quant_sig_8bit, step_size_8bit = quant_instance.quantize_signal(wave_array, 8)

dequant_instance = Dequantization()
dequant_sig_16bit = dequant_instance.dequantize(quant_sig_16bit, 16, step_size_16bit)
dequant_sig_8bit = dequant_instance.dequantize(quant_sig_8bit, 8, step_size_8bit)

# Plotting these signals you can see the impact of quantization
plt.plot(dequant_sig_8bit[:, 1])
plt.plot(wave_array[:, 1])